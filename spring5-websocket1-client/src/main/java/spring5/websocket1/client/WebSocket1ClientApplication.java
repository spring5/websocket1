package spring5.websocket1.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSocket1ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebSocket1ClientApplication.class, args);
	}
}
