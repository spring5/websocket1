/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring5.websocket1.client.shell;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.logging.Logger;
import org.springframework.http.MediaType;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Mono;
import spring5.websocket1.domain.Greeting;

/**
 *
 * @author harry
 */
@ShellComponent
public class WebSocketClientCommands {

  private static final Logger logger = Logger.getLogger(WebSocketClientCommands.class.getName());

  @ShellMethod("Connect to the WebSocket Service.")
  public String greet() {

    WebSocketClient client = new ReactorNettyWebSocketClient();
    EmitterProcessor<String> output = EmitterProcessor.create();

    try {
      int port = 7003;
      String url = String.format("ws://localhost:%d", port);
      String path = "/greeting";
      String targetUrl = url + path;
      logger.info(">>> targetUrl = " + targetUrl);
      URI uri = new URI(targetUrl);

      client.execute(uri, session -> {
        // return session.receive().map(WebSocketMessage::getPayloadAsText).subscribeWith(output).then();
        return session.receive().map(WebSocketMessage::getPayloadAsText).log().then();
      }).block(Duration.ofSeconds(10L));
    } catch (Exception ex) {
      logger.warning("Exception: " + ex.toString());
    }

    // ????
    return "OK";
  }
}
