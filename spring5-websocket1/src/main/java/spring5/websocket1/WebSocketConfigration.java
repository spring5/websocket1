package spring5.websocket1;

import java.util.Map;
import java.util.function.Consumer;
import java.time.Duration;
import java.util.HashMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SynchronousSink;
import spring5.websocket1.domain.Greeting;

@Configuration
public class WebSocketConfigration {

  @Bean
  WebSocketHandlerAdapter getHandlerAdapter() {
    return new WebSocketHandlerAdapter();
  }

  @Bean
  HandlerMapping getHandlerMapping() {
    SimpleUrlHandlerMapping hm = new SimpleUrlHandlerMapping();
    int order = 10;
    hm.setOrder(order);
    Map<String, WebSocketHandler> map = new HashMap<>();
    map.put("/greeting", getWebSocketHandler());
    hm.setUrlMap(map);
    return hm;
  }

  @Bean
  WebSocketHandler getWebSocketHandler() {
    return session -> {
      Flux<WebSocketMessage> publisher = Flux.interval(Duration.ofSeconds(1))
          .map(l -> String.format("{\"greeting\": \"Hello. It's %d.\"}", l)).map(session::textMessage);
      return session.send(publisher);
    };
  }

}
