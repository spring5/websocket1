package spring5.websocket1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSocket1Application {

	public static void main(String[] args) {
		SpringApplication.run(WebSocket1Application.class, args);
	}
}
