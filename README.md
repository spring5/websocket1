# Getting Started (Gradle)


## Scaffold

    spring init --name=WebSocket1 -g=spring5 -a=websocket1 -d=webflux,devtools,actuator --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-websocket1
    spring init --name=WebSocket1Client -g=spring5 -a=websocket1.client -d=spring-shell,webflux,devtools --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-websocket1-client



## Build

    ./gradlew clean build



## Run

    ./gradlew bootRun





